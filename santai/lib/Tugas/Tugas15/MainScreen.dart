import 'package:flutter/material.dart';
import 'package:sanberappmobile/Tugas/Tugas15/AccountScreen.dart';
import 'package:sanberappmobile/Tugas/Tugas15/SearchScreen.dart';
import 'HomeScreen.dart';


class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  final _layoutPage =[
    HomeScreen(),
    SearchScreen(),
    AccountScreen(),
  ];
  void _onTabItem(int index){
    setState((){
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(  
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home")
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text("Search")
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_sharp),
            title: Text("Account")
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}